import java.net.Socket;
import java.net.UnknownHostException;
import java.io.*;
import java.util.Iterator;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.Map;
import java.util.Objects;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import sun.security.tools.PathList;

/**
 * A simple net crawler for searching keywords
 * @author Dimitris Lygizos
 */
public class CrawlerLogic
{
    static String keyword;                               // current keyword
    static final String USERAGENT = "DLY-Crawler";              // user-agent (required by webpages for logging)
    static final Logger LOGGER = Logger.getLogger("Crawler");   // output logging
    // store found links (key is depth, value is linkedlist of reference URLs)
    static final TreeMap<Integer, TreeSet<urlnode>> Links = new TreeMap<>();

    private int recurse;            // how deep to search
    private Writer utp = null;      // write pages file
    private Writer kwrds = null;    // write keywords file
    private String status;
    private String currentHost;     // server of current under-process webpage

    private class urlnode implements Comparable {

        private String host;
        private String path;

        public urlnode(String host, String path)
        {
            this.host = host;
            this.path = path;
        }

        public String getHost() {
            return host;
        }

        public String getPath() {
            return path;
        }

        @Override
        public boolean equals(Object o)
        {
            if(this == o)   // self check
                return true;
            if(o == null)   // null check
                return false;
            if( this.getClass() != o.getClass() )
                return false;

            urlnode other = (urlnode) o;
            return this.host.equalsIgnoreCase(other.host) &&
                    this.path.equalsIgnoreCase(other.path);
        }

        @Override
        public int hashCode() {
            int hash = 7;
            hash = 67 * hash + Objects.hashCode(this.host);
            hash = 67 * hash + Objects.hashCode(this.path);
            return hash;
        }

        @Override
        public int compareTo(Object o) {
            if(this == o)   // self check
                return 0;
            if(o == null)
                throw new NullPointerException();

            urlnode other = (urlnode) o;

            if(this.host.compareToIgnoreCase(other.host) == 0)
                return this.path.compareToIgnoreCase(other.path);
            return this.host.compareToIgnoreCase(other.host);
        }
    }
    
    public CrawlerLogic(String keyword, String startUrl, int recurse) throws IOException
    {
        this.keyword = keyword;
        this.recurse = recurse;

        // setup files
        utp = new OutputStreamWriter(new FileOutputStream("Pages.txt"));        // found pages
        kwrds = new OutputStreamWriter(new FileOutputStream("KEYWORDS.txt"));   // found keywords
        utp.write("Session Start ");
        utp.write("\n");
        kwrds.write("Session Start ");
        kwrds.write("\n");

        String[] connectionInfo = parseUrl(startUrl);     // extract valuable information from URL string
        Links.put(1, new TreeSet<urlnode>());
        Links.get(1).add(new urlnode(connectionInfo[0], connectionInfo[1]));
    }

    /**
     * 
     */
    public void start() {
        Map.Entry<Integer, TreeSet<urlnode>> entry;
        // while the list of the website url's isn't empty
        while( (entry = Links.pollFirstEntry()) != null )  // get the next depth
        {
            Integer key = entry.getKey();                   // get depth
            TreeSet<urlnode> value = entry.getValue();      // get reference URLs
            while( !value.isEmpty() ) {
                // remove and process each URL in depth=key
                urlnode url = value.pollFirst();
                System.out.println(url.getHost() + url.getPath());
                // search this url
                linker(key, url.getHost(), url.getPath());    // recall the function for fresh "blood"
            }
        }
        
        try {
            utp.write("Session end");
            utp.write("\n");
            kwrds.write("Session end");
            kwrds.write("\n");
        } catch (IOException ex) {
            LOGGER.severe( "Error writing files " + ex.toString() );
        } finally {
            try {
                utp.close();
                kwrds.close();
            } catch (IOException ex) {
                LOGGER.severe( "Error closing filestreams " + ex.getMessage() );
            }
        }
    }

    /**
     * parse a url string and extract it's entities
     * @param url URL address string
     * @return hostname, relative path
     */
    private String[] parseUrl(String url)
    {
        String host = "";                   // set server hostname
        String relpath = "";                // set relative path
        String[] pack = new String[2];
        String prefix = "";
        pack[0] = pack[1] = null;

        // ignore protocol prefix
        if( url.startsWith("http://") ) {
            prefix = url.substring(0, 7);
            url = url.substring(7);
        } else if( url.startsWith("https://") ) {
            prefix = url.substring(0, 8);
            url = url.substring(8);
        }
        // tokenize url string by dots and split into domain and path
        StringTokenizer tokenizer = new StringTokenizer(url, "/", true);
        if (tokenizer.hasMoreTokens()) {
            String part = tokenizer.nextToken();
            StringTokenizer tokenizer2 = new StringTokenizer(part, ".", false);
            if (tokenizer2.countTokens() > 1) {
                relpath = url.substring(part.length());
                host = part;
            } else {
                relpath = url;
            }
        }
        if(!relpath.startsWith("/"))
            relpath = "/" + relpath;
        if (host.isEmpty())
            pack[0] = this.currentHost;
	else
            pack[0] = host;
        pack[1] = relpath;
        return pack;
    }

    public void linker(int depth, String host, String path)
    {
        String GetReq;
        boolean keywordFound = false;
        Socket clientSocket = null;

        try
        {
            // Connect to the socket in port 80;
            clientSocket = new Socket(host, 80);
            this.currentHost = clientSocket.getRemoteSocketAddress().toString();
            this.currentHost = this.currentHost.substring(0, this.currentHost.indexOf('/'));
            LOGGER.info("Connected in " + currentHost);

            // clientSocket.connect(null);
            GetReq = "GET " + path + " HTTP/1.1\r\n";       // Create a valid GET request header
            GetReq += "Host: " + host + "\r\n";
            GetReq += "User-Agent: " + USERAGENT + "\r\n";
            GetReq += "Connection: keep-alive\r\n";
            GetReq += "\r\n";
            LOGGER.fine(GetReq);

            sendData(clientSocket, GetReq);                 // send request
            keywordFound = getData(clientSocket, depth);
            utp.append("PAGE: \thost = ");
            utp.append(host);
            utp.append("; port = 80; relative_path = ");
            utp.append(path);
            utp.append("; status = ");
            utp.append(status);
            utp.append("\n");
            if((status.substring(9, 12)).equals("200")) {               // 200 = OK
                if(keywordFound) kwrds.append("KEYWORD_FOUND: host=");
                else kwrds.append("KEYWORD_NOT_FOUND: host=");
            }
            else kwrds.append("KEYWORD_NOT_SEARCHED: host=");
            kwrds.append(host);
            kwrds.append("; port = 80; relative_path = ");
            kwrds.append(path);
            kwrds.append("\n");
            clientSocket.shutdownInput();
            clientSocket.shutdownOutput();
        } catch (UnknownHostException e) {
            LOGGER.warning("Unknown host <" + host + ">");
        } catch (IOException e) {
            LOGGER.warning( e.toString() );
        } finally {
            try { clientSocket.close(); }   // close the socket
            catch (Exception e) {}
        }
    }

    /**
     * Send data to remote server
     * @param sock - an active server socket
     * @param data - Data to be send
     */
    public void sendData(Socket sock, String data)
    {
        try {
            OutputStream outToServer = sock.getOutputStream();
            OutputStreamWriter out = new OutputStreamWriter(outToServer);
            out.write(data);
            out.flush();
            outToServer.flush();
        } catch (IOException e) {
            LOGGER.log(Level.WARNING, "IOError at sending data to {0}", sock.getRemoteSocketAddress().toString());
        }
    }

    /**
     * Get response and content from the server and process them
     * @param sock - a server socket
     * @param key - keyword to search for
     * @return if keyword is in the server or not
     */
    public boolean getData(Socket sock, int depth)
    {
        boolean flg = false;
        String serverSentence;
        try {
            BufferedReader inFromServer = new BufferedReader(new InputStreamReader(sock.getInputStream()));
            status = inFromServer.readLine();
            LOGGER.finer(status);
            while((serverSentence = inFromServer.readLine()) != null) { // Read each line seperately
                if( findInSentence(serverSentence) ) flg = true;
                // if sentence contains a webpage reference, put this location into Links
                if( depth+1 <= recurse && serverSentence.contains("href") ) siteLinks(serverSentence, depth+1);
            }

        /* In Case that something will go wrong do nothing simply return the result at the end */
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println(e.toString());
        } finally {
            return flg;
        }
    }

    public boolean findInSentence(String sentence)
    {
        // search for the keyword
        return sentence.contains(keyword);  // return if keyword into sentence or not
    }

    /**
     * Put a new URL location into the Links TreeMap
     * @param sentence input string
     */
    public void siteLinks(String sentence, int depth)
    {
        int l=0, k = 0;
        while(true) {
            // find first occurence of <a href=
            k = sentence.indexOf("href=", k);
            if(k < 0) break; // if no link is found exit
            // add by the length of href
            k += 6;
            // resolve the hostname + directory position
            if ((l = sentence.indexOf('"', k)) == -1)
                l=sentence.length();

            // add to treeMap
            String[] connectionInfo = parseUrl( sentence.substring(k, l) );
            urlnode link = new urlnode(connectionInfo[0], connectionInfo[1]);
            if (Links.containsKey(depth))       // key already exists
                Links.get(depth).add(link);
            else {                              // insert new key
                TreeSet<urlnode> arg1 = new TreeSet<>();
                arg1.add(link);
                Links.put(depth, arg1);
            }
        }
    }
}
