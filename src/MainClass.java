import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Main for my custom search crawler
 * @author Dimitris Lygizos
 */
public class MainClass {
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        Logger logger = Logger.getLogger("Crawler");    // set information output logging
        logger.setLevel(Level.FINEST);

        if( args.length != 2) {     // command line arguments validation check
            logger.warning("you should provide 2 arguments. -> URL, -> keyword");
            return;
        }

        String startUrl = args[0];  // get url
        String keyword = args[1];   // get keyword

        // if keyword is too big
        if(keyword.length() > 50) {
            System.out.println("Keyword is way too big, program refuse to continue.");
            return;
        }

        System.out.println("In which depth would you like to search for the keyword? (Press ctrl+D to stop feed)");
        InputStreamReader r = null;
        try {
            int recurse;
            // read number from stdin
            r = new InputStreamReader(System.in);
            char[] tmp = new char[15];
            int c = r.read(tmp);        // read until IOException or EOF.

            try { recurse = Integer.parseInt( new String(tmp).trim() ); }
            catch( Exception ex ) {
                logger.warning("Number required!");
                return;
            }
            CrawlerLogic cl = new CrawlerLogic(keyword, startUrl, recurse);
            cl.start();
        } catch(Exception ex) {
            logger.severe(ex.toString() );
            return;
        } finally {
            if(r != null) {
                try { r.close(); }
                catch(Exception e) {}
            }
        }
    }
}
